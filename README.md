# Entrega do Processo de Integração Contínua 

---

## Resumo 

Objetivo dessa entrega é criar um processo de Integração Contínua, derivado de um fork do Projeto [Demo Aplicação NodeJs - Benc-UK](https://github.com/benc-uk/nodejs-demoapp). 

---

## Instalação 

Foram necessários para execução deste projeto: 

- **'Fork Project'** - Através de: [Demo Aplicação NodeJs - Benc-UK](https://github.com/benc-uk/nodejs-demoapp).
- **'Git'** - Ter o git configurado localmente para as criações e edições.
- **'IDE'** - [VSCode](https://code.visualstudio.com/). 

---

## Como Usar 

Este projeto está dividido em 3 partes:

* Subida do projeto no Bitbucket 
* Configuração do SonarCloud 
* Descrição e Criação do Pipeline 

---

### Subida do Projeto Bitbucket

Pode-se utilizar uma conta já existente ou criar uma nova conta através do [Bitbucket.org](https://bitbucket.org/account/signup/).

O projeto original está no GitHub, portanto foi necessário migrá-lo para o Bitbucket. 
O Bitbucket disponibiliza uma forma de realizar a importação de repositórios do GitHub e do GitLab diretametne pela ferramenta: 


		Create > 
			Import > 
				Repository 

Porém essa funcionalidade estava indisponível no momento da atividade, sendo assim, foi efetuado criação de um novo repositório e copia do projeto original para o novo repositório criado: 


1. Opção "Create Repository" no Bitbucket; 
2. Efetuar o clone do novo repositório para uma área local (`git clone`); 
3. Copiar conteúdo do projeto original para o novo repositório; 
4. Atualizar novo repositório (`git push`).  


--- 

### Configuração do SonarCloud 

1. Habilitar o plugin do SonarCloud no serviço do Bitbucket: 

		workspace > 
			Settings > 
				Marketplace > 
					SonarCloud 



2. Criar conta no SonarCloud. 

		* Pode-se usar alguma que já possua ou criar uma nova conforme indicação que você receberá em tela. 


3. Definir nome do Projeto no SonarCloud (utilizei o mesmo nome do repositório para facilitar o entendimento). 


4. Neste momento o SonarCloud irá lhe apresentar o Token a ser configurado no Bitbucket. 

		* Esse item é importante para que o Bitbucket consiga efetuar a chamada automática do SonarCloud durante a execução da Pipeline.  


5. Criar variável do Token Sonar no Bitbucket Repository: 


		Workspace > 
			Project > 
				Repository > 
					Repository settings > 
						Repository variables 



6. Aproveite este momento para também adicionar no `Repository variables` as variáveis de login no Docker Hub para push da image; 


		DOCKER_HUB_ID 
		DOCKER_HUB_PASSWORD 
		

--- 


### Descrição e Criação do Pipeline 

A definição e descrição da Pipeline de Build no Bitbucket é realizado através do arquivo `bitbucket-pipelines.yml`. 

Neste pipeline, há 4 etapas, sendo: 

1. **'Teste unitário'** - Execução do teste unitário previamente definido no projeto original; 
2. **'Testes Sonar'** - Execução do SonarCloud Analisys e SonarCloud Quality gate; 
3. **'Versionamento do Repositório'** - Execução do cadastro de tags versionando a release através da variável de Build do Bitbucket $BITBUCKET_BUILD_NUMBER; 
4. **'Build e Upload no Docker Hub'** - Execução do build e em seguida o push da imagem para o Docker hub, usando como tag o valor da variável $BITBUCKET_BUILD_NUMBER. Execução também da atualização da tag latest para a ultima imagem publicada. 

Foi utilizado a função **parallel** para execução dos steps 1 e 2. 

---

## Entregas 

* [Repositório Bitbucket](https://bitbucket.org/juferreiracb/nodejs-demoapp/src/master/) 
* [Pipeline Bitbucket](https://bitbucket.org/juferreiracb/nodejs-demoapp/addon/pipelines/home) 
* [Projeto SonarCloud](https://sonarcloud.io/dashboard?id=juferreiracb_nodejs-demoapp) 
* [Image DockerHub](https://hub.docker.com/repository/docker/juferreira/nodejs-demoapp)

.

* Observação com relação a execução do teste de Quality Gate pelo Sonar: 
	* O código está com alerta crítico para os itens `Vulnerabilities` e `Security Hotspots`. Para fins de conclusão do desafio, efetuei a edição das `Conditions`, removendo a `Metric Security Rating` e utilizando esse novo cenário de testes para seguir com a Pipeline. 

		`Organization` > 
			`Quality Gates` > 
				`Copy` > 
					`Edit` > 
						`Save` 

---

## Tecnologias

- **'Git'** - Para controle de versões do código; 
- **'Bitbucket'** - Para armazenamento do repositório remoto e para criação da Pipeline de Build; 
- **'SonarCloud'** - Para análise de código; 
- **'Docker'** - Para execução do código, testes do build local e para geração da image; 
- **'Docker Hub'** - Para armazenamento da image com controle de versão.

---

## Referencias 
[Bitbucket.org](https://bitbucket.org/bitbucketpipelines/) 

O arquivo `README.md` do projeto original foi alterado para `README-ORIGIN.md` e encontra-se no diretório raiz deste projeto. 

---

## Autor
Juliana Ferreira Matos. Me encontre no [LinkedIn](https://www.linkedin.com/in/juferreira/). 